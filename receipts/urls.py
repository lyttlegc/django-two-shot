from django.urls import path
from receipts.views import (
    show_receipts,
    create_receipt,
    expense_categories,
    accounts,
    create_category,
    create_account,
)

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_categories, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", accounts, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
