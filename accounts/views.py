from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
# Allows a user to login and redirects them to the home page
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect('home')
    else:
        form = LoginForm
    context = {
        'form' : form,
    }
    return render(request, "accounts/login.html", context)


# Allows a user to logout
def user_logout(request):
    logout(request)
    return redirect('login')

# Allows someone to signup and become a user
def signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)

            return redirect('home')
        else:
            form.add_error('password', 'the passwords do not match')
    else:
        form = SignupForm()
    context = {
        'form': form,
    }
    return render(request, "accounts/signup.html", context)
